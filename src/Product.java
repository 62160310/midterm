
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bunny0_
 */
public class Product implements Serializable{
    private String id;
    private String name;
    private String brand;
    private double price;
    private int amount;
    
    public Product(String id, String name, String brand, double price, int amount){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
        
    }
    public String getid() {
        return id;
    }

    public void setid(String id) {
        this.id = id;
    }
    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    public String getbrand() {
        return brand;
    }

    public void setbrand(String brand) {
        this.brand = brand;
    }
    public double getprice() {
        return price;
    }

    public void setprice(double price) {
        this.price = price;
    }
    public int getamount() {
        return amount;
    }

    public void setamount(int amount) {
        this.amount = amount;
    }
    
    
    
    
}
